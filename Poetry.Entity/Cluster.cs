﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
 
    /// <summary>
    /// 集群
    /// </summary>
    public class Cluster:BaseEntity
    {
        /// <summary>
        /// 集群ID
        /// </summary>
        [Required]
        [MaxLength(36)]
        public string  ClusterId
        {
            get { return Id; }
            set { Id = value; }
        }
        /// <summary>
        /// 集群名称
        /// </summary>
        [Required]
        public string ClusterName { get; set; }
        /// <summary>
        /// 集群Ip
        /// </summary>
        [Required]
        public string ClusterIp { get; set; }
        /// <summary>
        /// 集群端口
        /// </summary>
        [Required]
        public int ClusterPort { get; set; }
        /// <summary>
        /// 集群权重
        /// </summary>
        public string ClusterWeight { get; set; }
        /// <summary>
        /// 集群MasterId
        /// </summary>
        [Required]
        public string ClusterLeaderId { get; set; }
        /// <summary>
        /// 集群状态
        /// </summary>
        public int State { get; set; }
    }
    /// <summary>
    /// 集群状态
    /// </summary>
    public enum ClusterState {
        /// <summary>
        /// 存活
        /// </summary>
        Alive=0,
        /// <summary>
        /// 下线
        /// </summary>
        Dead=1
    }
}
