﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
    /// <summary>
    /// 基类
    /// </summary>
   public class BaseEntity
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        public char Deleted { get; set; } = '0';
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
    }
}
