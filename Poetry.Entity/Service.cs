﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
    /// <summary>
    /// 服务类
    /// </summary>
   public class Service : BaseEntity
    {
        /// <summary>
        /// 服务Id
        /// </summary>
        [Required]
        [MaxLength(36)]
        public string ServiceId
        {
            get { return Id; }
            set { Id = value; }
        }
        /// <summary>
        /// 服务名称
        /// </summary>
        [Required]
        public string ServiceName { get; set; }
        /// <summary>
        /// 服务元数据
        /// </summary>
        public string ServiceMetadata { get; set; }
        /// <summary>
        /// 服务命名空间
        /// </summary>
        public string ServiceNameSpace { get; set; }
    }
}
