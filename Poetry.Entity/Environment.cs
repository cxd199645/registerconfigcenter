﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
    /// <summary>
    /// 环境
    /// </summary>
   public class Environment:BaseEntity
    {
        /// <summary>
        /// 环境Id
        /// </summary>
        [Required]
        [MaxLength(36)]
        public string EnvironmentId
        {
            get { return Id; }
            set { Id = value; }
        }
        /// <summary>
        /// 环境名称
        /// </summary>
        [Required]
        public string EnvironmentName { get; set; }
        /// <summary>
        /// 环境描述
        /// </summary>
        public string EnvironmentDesc { get; set; }
    }
}
