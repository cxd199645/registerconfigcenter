﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
    /// <summary>
    /// 命名空间
    /// </summary>
    public class NameSpace: BaseEntity
    {
        /// <summary>
        /// 命名空间Id
        /// </summary>
        [Required]
        [MaxLength(36)]
        public string NameSpaceId
        {
            get { return Id; }
            set { Id = value; }
        }
        /// <summary>
        /// 命名空间名称
        /// </summary>
        [Required]
        public string NameSpaceName { get; set; }
        /// <summary>
        /// 命名空间描述
        /// </summary>
        public string NameSpaceDesc { get; set; }
     
    }
}
