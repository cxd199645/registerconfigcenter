﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Entity
{
    /// <summary>
    /// 配置
    /// </summary>
  public  class Config:BaseEntity
    {
        /// <summary>
        /// 配置Id
        /// </summary>
        [Required]
        [MaxLength(36)]
        public string ConfigId
        {
            get { return Id; }
            set { Id = value; }
        }
        /// <summary>
        /// 配置名称
        /// </summary>
        [Required]
        public string ConfigName { get; set; }
        /// <summary>
        /// 配置描述
        /// </summary>
        public string ConfigDesc { get; set; }
        /// <summary>
        /// 配置数据
        /// </summary>
        [Required]
        public string ConfigData { get; set; }
        /// <summary>
        /// 所属环境
        /// </summary>
        public string EnvironmentName { get; set; }
    }
}
