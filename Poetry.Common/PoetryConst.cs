﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common
{
    /// <summary>
    /// 常量
    /// </summary>
   public static class PoetryConst
    {
        /// <summary>
        /// 
        /// </summary>
        public const char Zero = '0';
        /// <summary>
        /// 
        /// </summary>
        public const char One = '1';
        /// <summary>
        /// 
        /// </summary>
        public const string DefaultService = "DefaultService";
        /// <summary>
        /// 
        /// </summary>
        public const string DefaultNameSpace = "DefaultNameSpace";
        /// <summary>
        /// 
        /// </summary>
        public const string DefaultEnvName="DefaultEnvironment";
    }
}
