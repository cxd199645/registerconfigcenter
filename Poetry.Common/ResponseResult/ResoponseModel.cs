﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common.ResponseResult
{
    /// <summary>
    /// WebApi response model
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Code
        /// </summary>
        public HttpStatusCode Code { get; set; } = HttpStatusCode.OK;
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; } = "Ok";
    }
    /// <summary>
    /// WebApi response model
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseModel<T> : ResponseModel
    {
        /// <summary>
        /// Result
        /// </summary>
        public T Result { get; set; }
    }
    public enum HttpStatusCode
    {
        /// <summary>
        /// Ok
        /// </summary>
        OK = 200,
        /// <summary>
        /// Created
        /// </summary>
        Created = 201,
        /// <summary>
        /// Accepted
        /// </summary>
        Accepted = 202,
        /// <summary>
        /// PartialContent
        /// </summary>
        PartialContent = 206,
        /// <summary>
        /// BadRequest
        /// </summary>
        BadRequest = 400,
        /// <summary>
        /// Unauthorized
        /// </summary>
        Unauthorized = 401,
        /// <summary>
        /// Forbidden
        /// </summary>
        Forbidden = 403,
        /// <summary>
        /// NotFound
        /// </summary>
        NotFound = 404,
        /// <summary>
        /// MethodNotAllowed
        /// </summary>
        MethodNotAllowed = 405,
        /// <summary>
        /// NotAcceptable
        /// </summary>
        NotAcceptable = 406,
        /// <summary>
        /// Conflict
        /// </summary>
        Conflict = 409,
        /// <summary>
        /// Gone
        /// </summary>
        Gone = 410,
        /// <summary>
        /// Locked
        /// </summary>
        Locked = 423,
        /// <summary>
        /// TooManyRequests
        /// </summary>
        TooManyRequests = 429,
        /// <summary>
        /// InternalServerError
        /// </summary>
        InternalServerError = 500,
        /// <summary>
        /// NotImplemented
        /// </summary>
        NotImplemented = 501,
        /// <summary>
        /// ServiceUnavailable
        /// </summary>
        ServiceUnavailable = 503,
        /// <summary>
        /// HTTPVersionNotSupported
        /// </summary>
        HTTPVersionNotSupported = 505
    }
}
