﻿using Poetry.Domain;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common.Mapper
{
    /// <summary>
    /// Mapper 扩展方法
    /// </summary>
    public static class MapperExtension
    {
        /// <summary>
        /// MapperConfig
        /// </summary>
        public static MapperConfig MapperConfig { get; set; }
        /// <summary>
        /// 转换
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="instance"></param>
        /// <param name="mapFrom"></param>
        /// <returns></returns>
        public static void Map<T, TR>(this T instance, TR mapFrom) where T:BaseEntity where TR:BaseDto
        {
            foreach (var info in instance.GetType().GetProperties())
            {
                var infoDto = mapFrom.GetType().GetProperties().Where(s => s.Name == info.Name).FirstOrDefault();
                if (infoDto is not null)
                {
                    info.SetValue(instance, infoDto.GetValue(mapFrom));
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="instance"></param>
        /// <param name="mapFrom"></param>
        public static void MapToDto<T, TR>(this T instance, TR mapFrom) where T : BaseDto where TR : BaseEntity
        {
            foreach (var info in instance.GetType().GetProperties())
            {
                var infoDto = mapFrom.GetType().GetProperties().Where(s => s.Name == info.Name).FirstOrDefault();
                if (infoDto is not null)
                {
                    info.SetValue(instance, infoDto.GetValue(mapFrom));
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="instance"></param>
        /// <param name="mapFrom"></param>
        public static void MapEntity<T, TR>(this T instance, TR mapFrom) where T : BaseEntity where TR : BaseEntity
        {
            foreach (var info in instance.GetType().GetProperties())
            {
                if (info.Name == "CreateDate") continue;
                var OldValue = info.GetValue(instance);
                if (info.Name.EndsWith("Id"))
                {
                    info.SetValue(instance, OldValue);
                    continue;
                }
                var infoDto = mapFrom.GetType().GetProperties().Where(s => s.Name == info.Name).FirstOrDefault();
                if (infoDto is not null)
                {
                    var NewValue = infoDto.GetValue(mapFrom);
                    if (NewValue != OldValue)
                    {
                        info.SetValue(instance, NewValue);
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="instance"></param>
        /// <param name="mapFrom"></param>
        public static void MapList<T, TR>(this List<T> instance, List<TR> mapFrom) where T : BaseEntity where TR : BaseDto
        {
            foreach (var infoInstance in instance)
            {
                var instanceDto = Activator.CreateInstance(typeof(TR)) as TR;
                foreach (var info in typeof(T).GetProperties())
                {
                    string sName = info.Name;
                    var infoDto =typeof(TR).GetProperties().Where(s => s.Name == info.Name).FirstOrDefault();
                    var value = info.GetValue(infoInstance);
                    if (infoDto is not null)
                    {
                        infoDto.SetValue(instanceDto, value);
                    }
                }
                mapFrom.Add(instanceDto);
            }
        }
    }
    /// <summary>
    /// MapperConfig
    /// </summary>
    public class MapperConfig {
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, Delegate> Actions = new Dictionary<string, Delegate>();
        /// <summary>
        /// MapperConfig
        /// </summary>
        public MapperConfig()
        {

        }
        /// <summary>
        /// Formember
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="action"></param>
        public MapperConfig FormemberEntity<T, TR>(Action<T, TR> action) where T : BaseEntity where TR : BaseDto
        {
            Actions.Add(typeof(T).Name, action);
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        public MapperConfig FormemberDto<T, TR>(Action<T, TR> action) where T : BaseDto where TR : BaseEntity
        {
            Actions.Add(typeof(T).Name, action);
            return this;
        }
    }
}
