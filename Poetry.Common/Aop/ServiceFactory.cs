﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common.Aop
{
    /// <summary>
    /// ServiceFactory
    /// </summary>
    public static class ServiceFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseType"></param>
        /// <param name="instanceType"></param>
        /// <param name="aopType"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        public static object CreateObj(Type BaseType,Type instanceType,Type aopType,IServiceProvider service)
        {
            var para = instanceType.GetConstructors().FirstOrDefault().GetParameters();
            var paraAop = aopType.GetConstructors().FirstOrDefault().GetParameters();
            List<object> list = new List<object>();
            List<object> listAop = new List<object>();
            foreach (var paraType in para)
            {
                var instance = service.GetService(paraType.ParameterType);
                list.Add(instance);
            }
            foreach (var paraType in paraAop)
            {
                var instance = service.GetService(paraType.ParameterType);
                listAop.Add(instance);
            }
            var result =DynamicProxy.Create(BaseType,instanceType, aopType, list.ToArray(), listAop.ToArray());
            return result;
        }
    }
}
