﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Poetry.Common.Json
{
    /// <summary>
    /// JsonHelper
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// 实体转Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static string EntityToJsonStr<T>(T instance)
        {
            try
            {
                return JsonSerializer.Serialize(instance);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
