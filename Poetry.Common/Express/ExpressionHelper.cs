﻿
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common.Express
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExpressionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="baseDTO"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GetExpression<T>(this T baseDTO) where T :BaseEntity
        {
            BinaryExpression result = null;
            Expression<Func<T, bool>> value = null;
            var lefttype = typeof(T);
            var rightType = baseDTO.GetType();
            var rightInfo = rightType.GetProperties();
            ParameterExpression parameter = Expression.Parameter(lefttype, "p");
            foreach (var memberInfo in lefttype.GetProperties())
            {
                var infoList = rightInfo.Where(s => s.Name == memberInfo.Name).ToList();
                if (infoList.Count> 0)
                {
                    var rightValue = infoList[0].GetValue(baseDTO);
                    if (rightValue.IsNullOrEmpty()) continue;
                    var LeftMember = Expression.Property(parameter, memberInfo);
                    var RightMember = Expression.Constant(rightValue);
                    BinaryExpression Result = null;
                    var attribute = memberInfo.GetCustomAttributes(true);
                    Result = Expression.Equal(LeftMember, RightMember);
                    if (result == null)
                    {
                        result = Result;
                    }
                    else
                    {
                        result = Expression.And(result, Result);
                    }
                }
            }
            value = Expression<Func<T, bool>>.Lambda<Func<T, bool>>(result, parameter);
            return value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TR"></typeparam>
        /// <param name="baseDTO"></param>
        /// <returns></returns>
        public static Func<TR, bool> GetExpression<T,TR>(this T baseDTO) where TR : BaseEntity
        {
            Func<TR, bool> func = new Func<TR, bool>(s => { return true; });
            foreach (var info in baseDTO.GetType().GetProperties())
            {
                var value = info.GetValue(baseDTO);
                if (!value.IsNullOrEmpty() && info.PropertyType != typeof(int))
                {
                    func += new Func<TR, bool>((s) =>
                    {
                        var property = s.GetType().GetProperty(info.Name);
                        if (property is not null)
                        {
                            if (info.PropertyType == typeof(string) && !property.GetValue(s).IsNullOrEmpty())
                            {
                                return property.GetValue(s).ToString().Equals(value.ToString());
                            }
                            else if (!property.GetValue(s).IsNullOrEmpty())
                            {
                                return property.GetValue(s).Equals(value);
                            }
                            else
                            { 
                            return false;

                            }
                        }
                        else
                        {
                            return false;
                        }
                    });
                }
                else if (info.PropertyType == typeof(int) && value != null && int.Parse(value.ToString()) > 0)
                {
                    func += new Func<TR, bool>((s) =>
                    {
                        var vValue = value;
                        var property = s.GetType().GetProperty(info.Name);
                        if (property is not null)
                        {
                            return property.GetValue(s).ToString() == vValue.ToString();
                        }
                        else
                        {
                            return false;
                        }
                    });
                }
            }
            return func;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this object str)
        {
            if (str == null) return true;
            else
            {
                return str.ToString().IsNullOrEmpty();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
    }
}
