﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Common.Config
{
    /// <summary>
    /// Json Config Entitu
    /// </summary>
    public class PoetryConfig
    {
        /// <summary>
        /// 模式（单机/集群）
        /// </summary>
        public Mode Mode { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public Role Role { get; set; }
        /// <summary>
        /// 集群
        /// </summary>
        public ClusterConfig Cluster { get; set; }
        /// <summary>
        /// 启动Url
        /// </summary>
        public string StartUrl { get; set; }
        /// <summary>
        /// 持久化方式
        /// </summary>
        public Persistence Persistence { get; set; }
        /// <summary>
        /// redis配置
        /// </summary>
        public RedisConfig RedisConfig { get; set; }
        /// <summary>
        /// 数据库配置
        /// </summary>
        public DBConfig DBConfig { get; set; }
    }
    /// <summary>
    /// 数据库配置
    /// </summary>
    public class DBConfig
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectString { get; set; }
    }
    /// <summary>
    /// 集群
    /// </summary>
    public class ClusterConfig
    {
        /// <summary>
        /// 注册的进群名称
        /// </summary>
        public string RegisterClusterName { get; set; }
        /// <summary>
        /// Ip
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// Port
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 权重
        /// </summary>
        public int? Weight { get; set; }
        /// <summary>
        /// 健康检查时间间隔
        /// </summary>
        public int HealthCheckInterval { get; set; } = 30;
    }
    /// <summary>
    /// redis配置
    /// </summary>
    public class RedisConfig
    {

        /// <summary>
        /// 单机 或哨兵 
        /// </summary>
        public RedisType RedisType { get; set; } = RedisType.Standalone;
        /// <summary>
        /// 链接字符串
        /// </summary>
        public string RedisConnectString { get; set; }
        /// <summary>
        /// 哨兵字符串
        /// </summary>
        public string[] Sentinels { get; set; }
        /// <summary>
        /// 是否只读
        /// </summary>
        public bool? ReadOnly { get; set; }
    }
    /// <summary>
    /// 数据持久化
    /// </summary>
    public enum Persistence
    {
        /// <summary>
        /// 内存
        /// </summary>
        Memory,
        /// <summary>
        /// MySql
        /// </summary>
        MySql,
        /// <summary>
        /// Redis
        /// </summary>
        Redis,
        /// <summary>
        /// SqlServer
        /// </summary>
        SqlServer
    }
    /// <summary>
    /// redis运行类型
    /// </summary>
    public enum RedisType
    {
        /// <summary>
        /// 单机(含主从)
        /// </summary>
        Standalone,
        /// <summary>
        /// 哨兵
        /// </summary>
        Sentinels
    }
    /// <summary>
    /// 模式
    /// </summary>
    public enum Mode
    {
        /// <summary>
        /// 单机
        /// </summary>
        Standalone,
        /// <summary>
        /// 集群
        /// </summary>
        Cluster
    }
    /// <summary>
    /// 角色
    /// </summary>
    public enum Role
    {
        /// <summary>
        /// 主
        /// </summary>
        Master,
        /// <summary>
        /// 从
        /// </summary>
        Slave
    }
}
