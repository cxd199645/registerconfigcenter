﻿
using Microsoft.Extensions.Logging;
using Poetry.Common.Aop;
using Poetry.Common.ResponseResult;
using Poetry.Domain;
using Poetry.Domain.Output.InstanceOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service
{
    public class ServiceAop : IInterceptor
    {
        private delegate object test(out object obj);
        public ServiceAop(ILogger<ServiceAop> logger)
        {
            Logger = logger;
        }

        public ILogger<ServiceAop> Logger { get; }

        public void AfterEvent(MethodInfo method, object result)
        {

        }

        public void BeforeEvent(MethodInfo method, object[] parameters)
        {

        }

        public object ExceptionEvent(Exception exception, MethodInfo method)
        {
            if (method.ReturnType.Name.Contains("Task"))
            {
                var func = new Func<ResponseModel>(() =>
                {
                    return new ResponseModel { Code = HttpStatusCode.InternalServerError, Message = exception.Message };
                });
                if (method.ReturnType.IsGenericType)
                {
                    var resultType = Expression.New(method.ReturnType.GetGenericArguments()[0]);
                    var resultFunc = Expression.Lambda(resultType).Compile().DynamicInvoke();
                    var expre = Expression.Call(typeof(Task), "FromResult", new Type[] { method.ReturnType.GetGenericArguments()[0] },Expression.Constant(resultFunc));
                    var func1 = Expression.Lambda<Func<object>>(expre).Compile()();
                    return func1;
                }
                var result = Activator.CreateInstance(method.ReturnType, func) as Task;
                result.Start();
                result.Wait();
                return result;
            }
            else
            {
                return new ResponseModel { Code = HttpStatusCode.InternalServerError, Message = exception.Message };
            }
        }
    }
}
