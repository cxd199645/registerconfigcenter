﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.EnvInputs;
using Poetry.Domain.Output.EnvOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
   public interface IEnvironmentService:IBaseService
    {
        Task<ResponseModel> InsertEnv(EnvironmentInput input);
        Task<ResponseModel<EnvironmentOutput>> QueryEnv(EnvironmentInput input);
        Task<ResponseModel<List<EnvironmentOutput>>> QueryListEnv(EnvironmentInput input);
        Task<ResponseModel> UpdateEnv(EnvironmentInput input);
        Task<ResponseModel> DeleteEnv(EnvironmentInput input);
    }
}
