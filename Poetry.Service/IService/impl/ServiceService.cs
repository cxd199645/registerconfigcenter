﻿using Poetry.Common;
using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.NameSpaceInputs;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Domain.Output.NameSpaceOutputs;
using Poetry.Domain.Output.ServiceOutputs;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    public class ServiceService : IServiceService
    {
        public ServiceService(IBaseRepository<Entity.Service> repository,INameSpaceService service,IBaseRepository<Instance> baseRepository)
        {
            Repository = repository;
            Service = service;
            BaseRepository = baseRepository;
        }

        public IBaseRepository<Entity.Service> Repository { get; }
        public INameSpaceService Service { get; }
        public IBaseRepository<Instance> BaseRepository { get; }

        public async Task<ResponseModel> CancelService(ServiceInput input)
        {
            var instance = new Entity.Service();
            instance.Map(input);
            var func = new Func<Entity.Service, bool>(s =>
            {
                return s.ServiceName == input.ServiceName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
                await BaseRepository.DeleteByExpression(s=> { return s.ServiceId == obj.ServiceId; });
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel<List<ServiceOutput>>> QueryListService(ServiceInput input)
        {
            ResponseModel<List<ServiceOutput>> model = new ResponseModel<List<ServiceOutput>>();
            var func = new Func<Entity.Service, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!input.ServiceName.IsNullOrEmpty())
            {
                list = list.Where(s => s.ServiceName == input.ServiceName).ToList();
            }
            if (!input.ServiceNameSpace.IsNullOrEmpty())
            {
                list = list.Where(s => s.ServiceNameSpace == input.ServiceNameSpace).ToList();
            }
            model.Result = new List<ServiceOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        public async Task<ResponseModel<ServiceOutput>> QueryService(ServiceInput input)
        {
            var model = new ResponseModel<ServiceOutput>();
            var func = input.GetExpression<ServiceInput, Entity.Service>();
            var result = await Repository.SelectByExpression(func);
            var modelResult = new ServiceOutput();
            if (result is not null)
            {
                modelResult.MapToDto(result);
                model.Result = modelResult;
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        public async Task<ResponseModel> RegisterService(ServiceInput input)
        {
            if (string.IsNullOrWhiteSpace(input.ServiceNameSpace) || input.ServiceNameSpace== PoetryConst.DefaultNameSpace)
            {
                input.ServiceNameSpace = PoetryConst.DefaultNameSpace;
                NameSpaceInput inputName = new NameSpaceInput();
                inputName.Id = Guid.NewGuid().ToString();
                inputName.NameSpaceName = input.ServiceNameSpace;
                var serviceNameSpace = await Service.QueryNameSpace(inputName);
                if (serviceNameSpace.Result is  null)
                {
                    await Service.InsertNameSpace(inputName);
                }
            }
            else
            {
                ResponseModel<NameSpaceOutput> output = await Service.QueryNameSpace(new  NameSpaceInput { NameSpaceName = input.ServiceNameSpace });
                if (output.Result is not null)
                {
                    input.ServiceNameSpace = output?.Result?.NameSpaceName;
                }
                else
                {
                    NameSpaceInput spaceInput = new NameSpaceInput();
                    spaceInput.Id = Guid.NewGuid().ToString();
                    spaceInput.NameSpaceName = input.ServiceNameSpace;
                    await Service.InsertNameSpace(spaceInput);
                }
            }
            var service = new Entity.Service();
            service.Map(input);
            service.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            await Repository.Save(service);
            return new ResponseModel();
        }

        public async Task<ResponseModel> UpdateService(ServiceInput input)
        {
            var instance = new Entity.Service();
            instance.Map(input);
            var func = new Func<Entity.Service, bool>(s =>
            {
                return  s.ServiceName == instance.ServiceName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
    }
}
