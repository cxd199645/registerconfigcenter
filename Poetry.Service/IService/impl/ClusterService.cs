﻿using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.ClusterInputs;
using Poetry.Domain.Output.ClusterOutputs;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    public class ClusterService : IClusterService
    {
        public ClusterService(IBaseRepository<Cluster> repository)
        {
            Repository = repository;
        }

        public IBaseRepository<Cluster> Repository { get; }

        public async Task<ResponseModel> DeleteCluster(ClusterInput clusterInput)
        {
            var instance = new Cluster();
            instance.Map(clusterInput);
            var func = new Func<Entity.Cluster, bool>(s =>
            {
                return s.ClusterName == clusterInput.ClusterName && s.ClusterPort == clusterInput.ClusterPort;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel> InsertCluster(ClusterInput clusterInput)
        {
            var isExist = await QueryCluster(new ClusterInput { ClusterName= clusterInput .ClusterName});
            if (isExist.Result is not null)
            {
                return new ResponseModel { Message = $"{clusterInput.ClusterName} Cluster is exist" };
            }
            var func = new Func<Entity.Cluster, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            var result = new Cluster() ;
            if (!clusterInput.MasterIp.IsNullOrEmpty())
            {
                result = list.FirstOrDefault(s => s.ClusterIp == clusterInput.MasterIp);
                var service = new Cluster();
                service.Map(clusterInput);
                service.ClusterLeaderId = result.ClusterId;
                service.State = 0;
                service.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                await Repository.Save(service);
            }
            else
            {
                var service = new Cluster();
                service.Map(clusterInput);
                service.State = 0;
                service.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                await Repository.Save(service);
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel<ClusterOutput>> QueryCluster(ClusterInput clusterInput)
        {
            ResponseModel<ClusterOutput> model = new ResponseModel<ClusterOutput>();
            var func = clusterInput.GetExpression<ClusterInput, Cluster>();
            var result = await Repository.SelectByExpression(func);
            model.Result = new ClusterOutput();
            if (result is not null)
            {
                model.Result.MapToDto(result);
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        public async Task<ResponseModel<List<ClusterOutput>>> QueryClusterList(ClusterInput input)
        {
            ResponseModel<List<ClusterOutput>> model = new ResponseModel<List<ClusterOutput>>();
            var func = new Func<Entity.Cluster, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!input.ClusterName.IsNullOrEmpty())
            {
                list = list.Where(s => s.ClusterName == input.ClusterName).ToList();
            }
            if (!input.MasterIp.IsNullOrEmpty())
            {
                var result = list.FirstOrDefault(s => s.ClusterIp == input.MasterIp);
                if (result is not null)
                {
                    list = list.Where(s => s.ClusterLeaderId == result.ClusterId).ToList();
                }
            }
            model.Result = new List<ClusterOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        public async Task<ResponseModel> UpdateCluster(ClusterInput clusterInput)
        {
            var instance = new Entity.Cluster();
            instance.Map(clusterInput);
            var func = new Func<Entity.Cluster, bool>(s =>
            {
                return s.ClusterName == clusterInput.ClusterName && s.ClusterPort == clusterInput.ClusterPort;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
    }
}
