﻿using Poetry.Common;
using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.ConfigInputs;
using Poetry.Domain.Input.EnvInputs;
using Poetry.Domain.Output.ConfigOutputs;
using Poetry.Domain.Output.EnvOutputs;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    public class ConfigService : IConfigService
    {
        public ConfigService(IBaseRepository<Config> repository,IEnvironmentService service)
        {
            Repository = repository;
            Service = service;
        }

        public IBaseRepository<Config> Repository { get; }
        public IEnvironmentService Service { get; }

        public async Task<ResponseModel> DeleteConfig(ConfigInput input)
        {
            var instance = new Config();
            instance.Map(input);
            var func = new Func<Config, bool>(s =>
            {
                return s.ConfigName==input.ConfigName&&s.EnvironmentName==input.EnvironmentName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel> InsertConfig(ConfigInput input)
        {

            if (input.EnvironmentName.IsNullOrEmpty())
            {
                input.EnvironmentName = PoetryConst.DefaultEnvName;
                EnvironmentInput environmentInput = new EnvironmentInput();
                environmentInput.EnvironmentName= PoetryConst.DefaultEnvName;
                environmentInput.Id = Guid.NewGuid().ToString();
                await Service.InsertEnv(environmentInput);
            }
            else
            {
                ResponseModel<EnvironmentOutput> model = await Service.QueryEnv(new EnvironmentInput { EnvironmentName=input.EnvironmentName}) ;
                if (model.Result is not null)
                {
                    input.EnvironmentName = model?.Result?.EnvironmentName;
                }
                else
                {
                    EnvironmentInput environmentInput = new EnvironmentInput();
                    environmentInput.EnvironmentName = PoetryConst.DefaultEnvName;
                    environmentInput.Id = Guid.NewGuid().ToString();
                    await Service.InsertEnv(environmentInput);
                    input.EnvironmentName = PoetryConst.DefaultEnvName;
                }
            }
            var isExist = await QueryConfig(new ConfigInput { ConfigName=input.ConfigName,EnvironmentName=input.EnvironmentName});
            if (isExist.Result is not null)
            {
                return new ResponseModel { Message = $"{input.ConfigName} Config is exist" };
            }
            var config = new Config();
            config.Map(input);
            config.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            await Repository.Save(config);
            return new ResponseModel();
        }

        public async Task<ResponseModel<ConfigOutput>> QueryConfig(ConfigInput instance)
        {
            ResponseModel<ConfigOutput> model = new ResponseModel<ConfigOutput>();
            var func = instance.GetExpression<ConfigInput, Config>();
            var result = await Repository.SelectByExpression(func);
            model.Result = new ConfigOutput();
            if (result is not null)
            {
                model.Result.MapToDto(result);
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        public async Task<ResponseModel<List<ConfigOutput>>> QueryConfigList(ConfigInput instance)
        {
            ResponseModel<List<ConfigOutput>> model = new ResponseModel<List<ConfigOutput>>();
            var func = new Func<Config, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!instance.ConfigName.IsNullOrEmpty())
            {
                list = list.Where(s => s.ConfigName == instance.ConfigName).ToList();
            }
            if (!instance.EnvironmentName.IsNullOrEmpty())
            {
                list = list.Where(s => s.EnvironmentName == instance.EnvironmentName).ToList();
            }
            if (!instance.ConfigDesc.IsNullOrEmpty())
            {
                list = list.Where(s => s.ConfigDesc == instance.ConfigDesc).ToList();
            }
            model.Result = new List<ConfigOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        public async  Task<ResponseModel> UpdateConfig(ConfigInput input)
        {
            var instance = new Config();
            instance.Map(input);
            var func = new Func<Config, bool>(s =>
            {
                return s.ConfigName == input.ConfigName && s.EnvironmentName == input.EnvironmentName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
    }
}
