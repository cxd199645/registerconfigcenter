﻿using Poetry.Common;
using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.EnvInputs;
using Poetry.Domain.Output.EnvOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    public class EnvironmentService : IEnvironmentService
    {
        public EnvironmentService(IBaseRepository<Entity.Environment> repository)
        {
            Repository = repository;
        }

        public IBaseRepository<Entity.Environment> Repository { get; }

        public async Task<ResponseModel> DeleteEnv(EnvironmentInput input)
        {
            var instance = new Entity.Environment();
            instance.Map(input);
            var func = new Func<Entity.Environment, bool>(s =>
            {
                return s.EnvironmentName == input.EnvironmentName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel> InsertEnv(EnvironmentInput input)
        {
            if (string.IsNullOrWhiteSpace(input.EnvironmentName))
            {
                input.EnvironmentName = PoetryConst.DefaultEnvName;
            }
            var isExist = await QueryEnv(new EnvironmentInput { EnvironmentName= input.EnvironmentName });
            if (isExist.Result is not null)
            {
                return new ResponseModel { Message = $"{input.EnvironmentName} Environment is exist" };
            }
            var service = new Entity.Environment();
            service.Map(input);
            service.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            await Repository.Save(service);
            return new ResponseModel();
        }

        public async Task<ResponseModel<EnvironmentOutput>> QueryEnv(EnvironmentInput input)
        {
            ResponseModel<EnvironmentOutput> model = new ResponseModel<EnvironmentOutput>();
            var func = input.GetExpression<EnvironmentInput, Entity.Environment>() ;
            var result = await Repository.SelectByExpression(func);
            var modelResult = new EnvironmentOutput();
            if (result is not null)
            {
                modelResult.MapToDto(result);
                model.Result = modelResult;
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        public async Task<ResponseModel<List<EnvironmentOutput>>> QueryListEnv(EnvironmentInput input)
        {
            ResponseModel<List<EnvironmentOutput>> model = new ResponseModel<List<EnvironmentOutput>>();
            var func = new Func<Entity.Environment, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!input.EnvironmentName.IsNullOrEmpty())
            {
                list = list.Where(s => s.EnvironmentName == input.EnvironmentName).ToList();
            }
            if (!input.EnvironmentDesc.IsNullOrEmpty())
            {
                list = list.Where(s => s.EnvironmentDesc == input.EnvironmentDesc).ToList();
            }
            model.Result = new List<EnvironmentOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        public async Task<ResponseModel> UpdateEnv(EnvironmentInput input)
        {
            var instance = new Entity.Environment();
            instance.Map(input);
            var func = new Func<Entity.Environment, bool>(s =>
            {
                return s.EnvironmentName == input.EnvironmentName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
    }
}
