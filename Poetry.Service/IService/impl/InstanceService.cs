﻿using Poetry.Common;
using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.InstanceInputs;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Domain.Output.InstanceOutputs;
using Poetry.Domain.Output.ServiceOutputs;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    internal class InstanceService : IInstanceService
    {
        public InstanceService(IBaseRepository<Instance> repository, IServiceService service, INameSpaceService nameSpaceService)
        {
            Repository = repository;
            Service = service;
        }

        public IBaseRepository<Instance> Repository { get; }
        public IServiceService Service { get; }

        /// <summary>
        /// CancelInstance
        /// </summary>
        /// <param name="instancedto"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CancelInstance(InstanceInput instancedto)
        {
            var instance = new Instance();
            instance.Map(instancedto);
            var func = new Func<Instance, bool>(s =>
            {
                return s.InstanceName == instance.InstanceName && s.ServiceName == instance.ServiceName && s.InstanceIp == instance.InstanceIp && s.InstancePort == instance.InstancePort;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
            }
            return new ResponseModel();
        }

        public async Task<ResponseModel<InstanceOutput>> GetInstance(InstanceInput instance)
        {
            ResponseModel<InstanceOutput> model = new ResponseModel<InstanceOutput>();
            var func = instance.GetExpression<InstanceInput, Instance>();
            var result = await Repository.SelectByExpression(func);
            model.Result = new InstanceOutput();
            if (result is not null)
            {
                model.Result.MapToDto(result);
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        /// <summary>
        /// GetInstanceArray
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public async Task<ResponseModel<List<InstanceOutput>>> GetInstanceArray(InstanceInput instance)
        {
            ResponseModel<List<InstanceOutput>> model = new ResponseModel<List<InstanceOutput>>();
            var func = new Func<Instance, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!instance.InstanceIp.IsNullOrEmpty())
            {
                list = list.Where(s => s.InstanceIp == instance.InstanceIp).ToList();
            }
            if (!instance.ServiceName.IsNullOrEmpty())
            {
                list = list.Where(s => s.ServiceName == instance.ServiceName).ToList();
            }
            if (!instance.InstancePort.IsNullOrEmpty() && instance.InstancePort > 0)
            {
                list = list.Where(s => s.InstancePort == instance.InstancePort).ToList();
            }
            if (!instance.InstanceName.IsNullOrEmpty())
            {
                list = list.Where(s => s.InstanceName == instance.InstanceName).ToList();
            }
            if (!instance.InstanceWeight.IsNullOrEmpty())
            {
                list = list.Where(s => s.InstanceWeight == instance.InstanceWeight).ToList();
            }
            model.Result = new List<InstanceOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        /// <summary>
        /// 服务注册
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public async Task<ResponseModel> RegisterInstance(InstanceInput instancedto)
        {
            string ServiceId = string.Empty;
            if (string.IsNullOrWhiteSpace(instancedto.ServiceName) || instancedto.ServiceName == PoetryConst.DefaultService)
            {
                instancedto.ServiceName = PoetryConst.DefaultService;
                ServiceInput input = new ServiceInput();
                input.Id = Guid.NewGuid().ToString();
                input.ServiceName = instancedto.ServiceName;
                ServiceId = input.Id;
                var service = await Service.QueryService(input);
                if (service.Result is not null)
                {
                    ServiceId = service.Result.Id;
                }
                else
                {
                    await Service.RegisterService(input);
                }
            }
            else
            {
                ResponseModel<ServiceOutput> output = await Service.QueryService(new ServiceInput { ServiceName = instancedto.ServiceName });
                if (output.Result is not null)
                {
                    instancedto.ServiceName = output?.Result?.ServiceName;
                    ServiceId = output.Result?.Id;
                }
                else
                {
                    ServiceInput input = new ServiceInput();
                    input.Id = Guid.NewGuid().ToString();
                    input.ServiceName = instancedto.ServiceName;
                    input.ServiceNameSpace = PoetryConst.DefaultNameSpace;
                    ServiceId = input.Id;
                    var service = await Service.QueryService(input);
                    if (service.Result is not null)
                    {
                        ServiceId = service.Result.Id;
                    }
                    else
                    {
                        await Service.RegisterService(input);
                    }
                }
            }
            var instance = new Instance();
            instance.Map(instancedto);
            instance.ServiceId = ServiceId;
            instance.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            await Repository.Save(instance);
            ResponseModel model = new ResponseModel();
            return model;
        }
        /// <summary>
        /// UpdateInstance
        /// </summary>
        /// <param name="instancedto"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateInstance(InstanceInput instancedto)
        {
            var instance = new Instance();
            instance.Map(instancedto);
            var func = new Func<Instance, bool>(s =>
            {
                return s.InstanceName == instance.InstanceName && s.InstanceIp == instance.InstanceIp && s.InstancePort == instance.InstancePort && s.ServiceName == instance.ServiceName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
    }
}
