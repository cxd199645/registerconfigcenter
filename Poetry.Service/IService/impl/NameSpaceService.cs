﻿using Poetry.Common;
using Poetry.Common.Express;
using Poetry.Common.Mapper;
using Poetry.Common.ResponseResult;
using Poetry.DataRepository.Repository;
using Poetry.Domain.Input.NameSpaceInputs;
using Poetry.Domain.Output.NameSpaceOutputs;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService.impl
{
    public class NameSpaceService : INameSpaceService
    {
        public NameSpaceService(IBaseRepository<NameSpace> repository)
        {
            Repository = repository;
        }

        public IBaseRepository<NameSpace> Repository { get; }

      

        public async Task<ResponseModel> InsertNameSpace(NameSpaceInput input)
        {
            if (string.IsNullOrWhiteSpace(input.NameSpaceName))
            {
                input.NameSpaceName = PoetryConst.DefaultNameSpace;
            }
            var isExist = await QueryNameSpace(new NameSpaceInput { NameSpaceName = input.NameSpaceName });
            if (isExist.Result is not null)
            {
                return new ResponseModel { Message = $"{input.NameSpaceName }NameSpace is exist" };
            }
            var service = new NameSpace();
            service.Map(input);
            service.CreateDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            await Repository.Save(service);
            return new ResponseModel();
        }

        public async Task<ResponseModel<List<NameSpaceOutput>>> QueryListNameSpace(NameSpaceInput input)
        {
            ResponseModel<List<NameSpaceOutput>> model = new ResponseModel<List<NameSpaceOutput>>();
            var func = new Func<Entity.NameSpace, bool>(s => { return true; });
            var list = await Repository.SelectListByExpression(func);
            if (!input.NameSpaceName.IsNullOrEmpty())
            {
                list = list.Where(s => s.NameSpaceName == input.NameSpaceName).ToList();
            }
            if (!input.NameSpaceDesc.IsNullOrEmpty())
            {
                list = list.Where(s => s.NameSpaceDesc == input.NameSpaceDesc).ToList();
            }
            model.Result = new List<NameSpaceOutput>();
            if (list is not null && list.Count > 0)
            {
                list.MapList(model.Result);
            }
            else
            {
                model.Result = null;
                model.Message = "list is empty";
            }
            return model;
        }

        public async Task<ResponseModel<NameSpaceOutput>> QueryNameSpace(NameSpaceInput input)
        {
            var model = new ResponseModel<NameSpaceOutput>();
            var func = input.GetExpression<NameSpaceInput, NameSpace>();
            var result = await Repository.SelectByExpression(func);
            var modelResult = new NameSpaceOutput();
            if (result is not null)
            {
                modelResult.MapToDto(result);
                model.Result = modelResult;
            }
            else
            {
                model.Result = null;
                model.Message = "Model is not exist!";
            }
            return model;
        }

        public async Task<ResponseModel> UpdateNameSpace(NameSpaceInput input)
        {
            var instance = new NameSpace();
            instance.Map(input);
            var func = new Func<NameSpace, bool>(s =>
            {
                return s.NameSpaceName == input.NameSpaceName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                obj.MapEntity(instance);
                await Repository.Update(obj);
            }
            ResponseModel model = new ResponseModel();
            return model;
        }
        public async Task<ResponseModel> DeleteNameSpace(NameSpaceInput input)
        {
            var instance = new NameSpace();
            instance.Map(input);
            var func = new Func<NameSpace, bool>(s =>
            {
                return s.NameSpaceName==input.NameSpaceName;
            });
            var obj = await Repository.SelectByExpression(func);
            if (obj is not null)
            {
                await Repository.Delete(obj);
            }
            return new ResponseModel();
        }
    }
}
