﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Domain.Output.ServiceOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
   public interface IServiceService:IBaseService
    {
        Task<ResponseModel> RegisterService(ServiceInput input);
        Task<ResponseModel<ServiceOutput>> QueryService(ServiceInput input);
        Task<ResponseModel> CancelService(ServiceInput input);
        Task<ResponseModel<List<ServiceOutput>>> QueryListService(ServiceInput input);
        Task<ResponseModel> UpdateService(ServiceInput input);
    }
}
