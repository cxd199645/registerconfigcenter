﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.NameSpaceInputs;
using Poetry.Domain.Output.NameSpaceOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
    public interface INameSpaceService:IBaseService
    {
        Task<ResponseModel> InsertNameSpace(NameSpaceInput input);
        Task<ResponseModel<NameSpaceOutput>> QueryNameSpace(NameSpaceInput input);
        Task<ResponseModel<List<NameSpaceOutput>>> QueryListNameSpace(NameSpaceInput input);
        Task<ResponseModel> UpdateNameSpace(NameSpaceInput input);
        Task<ResponseModel> DeleteNameSpace(NameSpaceInput input);
    }
}
