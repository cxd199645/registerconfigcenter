﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ConfigInputs;
using Poetry.Domain.Output.ConfigOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
   public interface IConfigService: IBaseService
    {
        Task<ResponseModel> InsertConfig(ConfigInput input);
        Task<ResponseModel<List<ConfigOutput>>> QueryConfigList(ConfigInput instance);
        Task<ResponseModel<ConfigOutput>> QueryConfig(ConfigInput instance);
        Task<ResponseModel> DeleteConfig(ConfigInput input);
        Task<ResponseModel> UpdateConfig(ConfigInput input);
    }
}
