﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.InstanceInputs;
using Poetry.Domain.Output.InstanceOutputs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
    public interface IInstanceService:IBaseService
    {
        Task<ResponseModel> RegisterInstance(InstanceInput instance);
        Task<ResponseModel> CancelInstance(InstanceInput instance);
        Task<ResponseModel> UpdateInstance(InstanceInput instance);
        Task<ResponseModel<List<InstanceOutput>>> GetInstanceArray(InstanceInput instance);
        Task<ResponseModel<InstanceOutput>> GetInstance(InstanceInput instance);
    }
}
