﻿using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ClusterInputs;
using Poetry.Domain.Output.ClusterOutputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Service.IService
{
    public interface IClusterService:IBaseService
    {
         Task<ResponseModel> InsertCluster(ClusterInput clusterInput);
        Task<ResponseModel<ClusterOutput>> QueryCluster(ClusterInput clusterInput);
        Task<ResponseModel<List<ClusterOutput>>> QueryClusterList(ClusterInput clusterInput);
        Task<ResponseModel> DeleteCluster(ClusterInput clusterInput);
        Task<ResponseModel> UpdateCluster(ClusterInput clusterInput);
    }
}
