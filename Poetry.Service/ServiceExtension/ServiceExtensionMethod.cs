﻿using CSRedis;
using Microsoft.Extensions.DependencyInjection;
using Poetry.Common.Aop;
using Poetry.Common.Config;
using Poetry.Common.Mapper;
using Poetry.DataRepository;
using Poetry.DataRepository.Repository;
using Poetry.DataRepository.Repository.impl;
using Poetry.Domain;
using Poetry.Domain.Input.InstanceInputs;
using Poetry.Domain.Input.NameSpaceInputs;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Entity;
using System.Linq;
using System.Reflection;

namespace Poetry.Service.ServiceExtension
{
    public static class ServiceExtensionMethod
    {
        public static IServiceCollection ConfigPersistence(this IServiceCollection services, PoetryConfig poetryConfig)
        {
            switch (poetryConfig.Persistence)
            {
                case Persistence.Redis:
                    services.AddScoped(typeof(IBaseRepository<>), typeof(RedisRepository<>));
                    break;
                case Persistence.Memory:
                    services.AddMemoryCache();
                    services.AddScoped(typeof(IBaseRepository<>), typeof(MemoryRepository<>));
                    break;
                default:
                    services.AddDbContext<BaseDbContext>();
                    services.AddScoped(typeof(IBaseRepository<>), typeof(DbContextRepository<>));
                    break;
            }
            services.AddPoetryService();
            services.AddCsRedis(poetryConfig);
            return services;
        }
        private static IServiceCollection AddPoetryService(this IServiceCollection services)
        {
            var allServiceType = Assembly.Load("Poetry.Service").GetTypes().Where(s => s.IsInterface && s != typeof(IBaseService)).ToList();
            var allType = Assembly.Load("Poetry.Service").GetTypes().Where(s => s.Namespace == "Poetry.Service.IService.impl").ToList();
            foreach (var type in allServiceType)
            {
                var instanceType = allType.Where(s => type.Name.Contains(s.Name)).FirstOrDefault();
                if (instanceType is not null)
                {
                    services.AddScoped(type, s =>
                    {
                        return ServiceFactory.CreateObj(type, instanceType,typeof(ServiceAop),s);
                     });
                }
            }
            return services;
        }
        private static IServiceCollection AddCsRedis(this IServiceCollection services, PoetryConfig poetryConfig)
        {
            CSRedisClient Client = null;
            switch (poetryConfig.RedisConfig.RedisType)
            {
                case RedisType.Standalone:
                    Client = new CSRedisClient(poetryConfig.RedisConfig.RedisConnectString);
                    break;
                case RedisType.Sentinels:
                    Client = new CSRedisClient(poetryConfig.RedisConfig.RedisConnectString, poetryConfig.RedisConfig.Sentinels, poetryConfig.RedisConfig.ReadOnly.Value);
                    break;
            }
            if (Client is not null)
            {
                services.AddSingleton(Client);
            }
            return services;
        }
    }
}
