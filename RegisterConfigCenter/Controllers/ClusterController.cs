﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ClusterInputs;
using Poetry.Domain.Output.ClusterOutputs;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ClusterController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clusterService"></param>
        public ClusterController(IClusterService clusterService)
        {
            ClusterService = clusterService;
        }
        /// <summary>
        /// 
        /// </summary>
        public IClusterService ClusterService { get; }
        /// <summary>
        /// 集群注册
        /// </summary>
        /// <returns></returns>
        [HttpPost("InsertCluster")]
        public async Task<ResponseModel> InsertCluster([FromBody] ClusterInput input)
        {
            input.Id = Guid.NewGuid().ToString();
            var model = await ClusterService.InsertCluster(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryCluster")]
        public async Task<ResponseModel<ClusterOutput>> QueryCluster([FromQuery] ClusterInput input)
        {
            var model = await ClusterService.QueryCluster(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryListCluster")]
        public async Task<ResponseModel<List<ClusterOutput>>> QueryListCluster([FromQuery] ClusterInput input)
        {
            var model = await ClusterService.QueryClusterList(input);
            return model;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpDelete("DeleteCluster")]
        public async Task<ResponseModel> DeleteCluster([FromQuery] ClusterInput input)
        {
            var model = await ClusterService.DeleteCluster(input);
            return model;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("UpdateCluster")]
        public async Task<ResponseModel> UpdateCluster([FromBody] ClusterInput input)
        {
            var model = await ClusterService.UpdateCluster(input);
            return model;
        }
    }
}
