﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Domain.Output.ServiceOutputs;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// ServiceController
    /// </summary>
    [Route("Service/v1")]
    public class ServiceController : ControllerBase
    {
        /// <summary>
        /// ServiceController
        /// </summary>
        public ServiceController(IServiceService service)
        {
            Service = service;
        }
        /// <summary>
        /// Service
        /// </summary>
        public IServiceService Service { get; }
        /// <summary>
        /// 服务注册
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("RegisterService")]
        public async Task<ResponseModel> RegisterService(ServiceInput input)
        {
            input.Id = Guid.NewGuid().ToString();
            return await Service.RegisterService(input);
        }
        /// <summary>
        /// 服务注销
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpDelete("CancelService")]
        public async Task<ResponseModel> CancelService([FromQuery] ServiceInput instance)
        {
            var result = await Service.CancelService(instance);
            return result;
        }
        /// <summary>
        /// 获取服务详情
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryService")]
        public async Task<ResponseModel<ServiceOutput>> QueryService([FromQuery] ServiceInput instance)
        {
            ResponseModel<ServiceOutput> model = await Service.QueryService(instance);
            return model;
        }
        /// <summary>
        /// 获取服务列表
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryListService")]
        public async Task<ResponseModel<List<ServiceOutput>>> QueryListService([FromQuery] ServiceInput instance)
        {
            ResponseModel<List<ServiceOutput>> model = await Service.QueryListService(instance);
            return model;
        }
        /// <summary>
        /// 服务更新
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpPut("UpdateService")]
        public async Task<ResponseModel> UpdateService([FromBody] ServiceInput instance)
        {
            var result = await Service.UpdateService(instance);
            return result;
        }
    }
}
