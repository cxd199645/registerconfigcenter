﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.ConfigInputs;
using Poetry.Domain.Output.ConfigOutputs;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("Config/v1")]
    public class ConfigController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ConfigController(IConfigService configService)
        {
            ConfigService = configService;
        }
        /// <summary>
        /// 
        /// </summary>
        public IConfigService ConfigService { get; }

        /// <summary>
        /// 配置增加
        /// </summary>
        /// <returns></returns>
        [HttpPost("InsertConfig")]
        public async Task<ResponseModel> InsertConfig([FromBody] ConfigInput configInput)
        {
            configInput.Id = Guid.NewGuid().ToString();
            ResponseModel model = await ConfigService.InsertConfig(configInput);
            return model;
        }
        /// <summary>
        /// 获取实例列表
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryConfigArray")]
        public async Task<ResponseModel<List<ConfigOutput>>> QueryConfigArray([FromQuery] ConfigInput instance)
        {
            ResponseModel<List<ConfigOutput>> model = await ConfigService.QueryConfigList(instance);
            return model;
        }
        /// <summary>
        /// 获取配置详情
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryConfig")]
        public async Task<ResponseModel<ConfigOutput>> QueryConfig([FromQuery] ConfigInput instance)
        {
            ResponseModel<ConfigOutput> model = await ConfigService.QueryConfig(instance);
            return model;
        }
        /// <summary>
        /// 注销配置
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpDelete("CancelConfig")]
        public async Task<ResponseModel> CancelConfig([FromQuery] ConfigInput instance)
        {
            var result = await ConfigService.DeleteConfig(instance);
            return result;
        }
        /// <summary>
        /// 更新配置
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpPut("UpdateConfig")]
        public async Task<ResponseModel> UpdateConfig([FromBody] ConfigInput instance)
        {
            var result = await ConfigService.UpdateConfig(instance);
            return result;
        }
    }
}
