﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.NameSpaceInputs;
using Poetry.Domain.Output.NameSpaceOutputs;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("NameSpace/v1")]
    public class NameSpaceController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        public NameSpaceController(INameSpaceService service)
        {
            Service = service;
        }
        /// <summary>
        /// 
        /// </summary>
        public INameSpaceService Service { get; }

        /// <summary>
        /// 增加命名空间
        /// </summary>
        /// <returns></returns>
        [HttpPost("InsertNameSpace")]
        public async Task<ResponseModel> InsertNameSpace([FromBody] NameSpaceInput input)
        {
            input.Id = Guid.NewGuid().ToString();
            var model = await Service.InsertNameSpace(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryNameSpace")]
        public async Task<ResponseModel<NameSpaceOutput>> QueryNameSpace([FromQuery] NameSpaceInput input)
        {
            var model = await Service.QueryNameSpace(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryListNameSpace")]
        public async Task<ResponseModel<List<NameSpaceOutput>>> QueryListNameSpace([FromQuery] NameSpaceInput input)
        {
            var model = await Service.QueryListNameSpace(input);
            return model;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpDelete("DeleteNameSpace")]
        public async Task<ResponseModel> DeleteNameSpace([FromQuery] NameSpaceInput input)
        {
            var model = await Service.DeleteNameSpace(input);
            return model;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("UpdateNameSpace")]
        public async Task<ResponseModel> UpdateNameSpace([FromBody] NameSpaceInput input)
        {
            var model = await Service.UpdateNameSpace(input);
            return model;
        }
    }
}
