﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common;
using Poetry.Common.ResponseResult;
using Poetry.Domain;
using Poetry.Domain.Input.InstanceInputs;
using Poetry.Domain.Input.ServiceInputs;
using Poetry.Domain.Output.InstanceOutputs;
using Poetry.Entity;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// 实例接口
    /// </summary>
    [Route("Instance/v1")]
    public class InstanceController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        public IInstanceService Service { get; }

        /// <summary>
        /// 构造方法
        /// </summary>
        public InstanceController(IInstanceService service)
        {
            Service = service;
        }

        /// <summary>
        /// 注册实例
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpPost("RegisterInstance")]
        public async Task<ResponseModel> RegisterInstance([FromBody] InstanceInput instance)
        {
            if (string.IsNullOrWhiteSpace(instance.Id))
            {
                instance.Id = Guid.NewGuid().ToString();
            }
            var result= await Service.RegisterInstance(instance);
            return result;
        }
        /// <summary>
        /// 注销实例
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpDelete("CancelInstance")]
        public async Task<ResponseModel> CancelInstance([FromQuery] InstanceInput instance)
        {
            var result = await Service.CancelInstance(instance);
            return result;
        }
        /// <summary>
        /// 更新实例
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpPut("UpdateInstance")]
        public async Task<ResponseModel> UpdateInstance([FromBody] InstanceInput instance)
        {
            var result = await Service.UpdateInstance(instance);
            return result;
        }
        /// <summary>
        /// 获取实例列表
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryInstanceArray")]
        public async Task<ResponseModel<List<InstanceOutput>>>QueryInstanceArray([FromQuery] InstanceInput instance)
        {
            ResponseModel<List<InstanceOutput>> model = await Service.GetInstanceArray(instance);
            return model;
        }
        /// <summary>
        /// 获取实例详情
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        [HttpGet("QueryInstance")]
        public async Task<ResponseModel<InstanceOutput>> QueryInstance([FromQuery] InstanceInput instance)
        {
            ResponseModel<InstanceOutput> model = await Service.GetInstance(instance);
            return model;
        }
    }
}
