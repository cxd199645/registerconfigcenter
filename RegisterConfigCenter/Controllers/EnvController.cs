﻿using Microsoft.AspNetCore.Mvc;
using Poetry.Common.ResponseResult;
using Poetry.Domain.Input.EnvInputs;
using Poetry.Domain.Output.EnvOutputs;
using Poetry.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("Env/v1")]
    public class EnvController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public EnvController(IEnvironmentService service)
        {
            Service = service;
        }
        /// <summary>
        /// 
        /// </summary>
        public IEnvironmentService Service { get; }
        /// <summary>
        /// 增加环境
        /// </summary>
        /// <returns></returns>
        [HttpPost("InsertEnv")]
        public async Task<ResponseModel> InsertEnv([FromBody] EnvironmentInput input)
        {
            input.Id = Guid.NewGuid().ToString();
            var model = await Service.InsertEnv(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryEnv")]
        public async Task<ResponseModel<EnvironmentOutput>> QueryEnv([FromQuery] EnvironmentInput input)
        {
            var model = await Service.QueryEnv(input);
            return model;
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        [HttpGet("QueryListEnv")]
        public async Task<ResponseModel<List<EnvironmentOutput>>> QueryListEnv([FromQuery] EnvironmentInput input)
        {
            var model = await Service.QueryListEnv(input);
            return model;
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpDelete("DeleteEnv")]
        public async Task<ResponseModel> DeleteEnv([FromQuery] EnvironmentInput input)
        {
            var model = await Service.DeleteEnv(input);
            return model;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("UpdateEnv")]
        public async Task<ResponseModel> UpdateEnv([FromBody] EnvironmentInput input)
        {
            var model = await Service.UpdateEnv(input);
            return model;
        }
    }
}
