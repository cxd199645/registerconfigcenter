﻿using CSRedis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Poetry.Common.Config;
using Poetry.Service.IService;
using Poetry.Service.ServiceExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api
{
    /// <summary>
    /// service扩展方法
    /// </summary>
    public static class ServiceExtensionMethod
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddPoetry(this IServiceCollection services,IConfiguration configuration)
        {
            var config = new PoetryConfig();
            ChangeToken.OnChange(() => configuration.GetReloadToken(), () =>
            {
                config = configuration.GetSection("Poetry").Get<PoetryConfig>();
                configuration.Bind(config);
            });
            config = configuration.GetSection("Poetry").Get<PoetryConfig>();
            configuration.Bind(config);
            services.AddOptions<PoetryConfig>();
            services.Configure<PoetryConfig>(configuration.GetSection("Poetry"));
            services.ConfigPersistence(config);
            return services;
        }
    }
}
