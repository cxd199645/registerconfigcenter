﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Poetry.Domain.DomainAttribute;
using System.Linq;

namespace Poetry.Api.Filter
{
    /// <summary>
    /// CheckParaFilter
    /// </summary>
    public class CheckParaFilter : ActionFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string sErrorMsg = string.Empty;
            foreach (var para in context.ActionArguments)
            {
                var valueType = para.Value.GetType();
                if (valueType.IsClass)
                {
                    foreach (var info in valueType.GetProperties())
                    {
                        var attribute = info.GetCustomAttributes(true).FirstOrDefault(s => s.GetType() == typeof(RequireParaAttribute)) as RequireParaAttribute;
                        if (attribute is not null && attribute.MethodName.Contains((context.ActionDescriptor as ControllerActionDescriptor)?.ActionName))
                        {
                            if (!attribute.IsValid(info.GetValue(para.Value)))
                            {
                                sErrorMsg += attribute.FormatErrorMessage(info.Name)+"\r\n";
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(sErrorMsg))
            {
                context.Result = new ObjectResult(sErrorMsg) { StatusCode = 400 };
            }
        }
    }
}
