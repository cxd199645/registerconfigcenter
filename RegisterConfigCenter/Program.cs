using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poetry.Api
{
    /// <summary>
    /// ����
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main ����
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseUrls(GetConfiguration()["StartUrl"]);
                    webBuilder.UseStartup<Startup>();
                });
        /// <summary>
        /// GetConfiguration
        /// </summary>
        /// <returns></returns>
        public static IConfigurationSection GetConfiguration()
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
          .AddJsonFile("appsettings.json")
          .Build();
            return configuration.GetSection("Poetry");
        }
    }
}
