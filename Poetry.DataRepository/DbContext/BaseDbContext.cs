﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Poetry.Common.Config;
using Poetry.DataRepository.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Poetry.DataRepository
{
  public  class BaseDbContext:DbContext
    {
        #region ctor
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="optionsSnapshot"></param>
        public BaseDbContext(IOptionsSnapshot<PoetryConfig> optionsSnapshot)
        {
            OptionsSnapshot = optionsSnapshot;
        }
        #endregion
        #region Configration Method
        public Persistence Persistence { get; }
        public IOptionsSnapshot<PoetryConfig> OptionsSnapshot { get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                switch (OptionsSnapshot.Value.Persistence)
                {
                    case Persistence.MySql:
                        optionsBuilder.UseMySql(OptionsSnapshot.Value.DBConfig.ConnectString, ServerVersion.AutoDetect(OptionsSnapshot.Value.DBConfig.ConnectString));
                        break;
                    case Persistence.SqlServer:
                        optionsBuilder.UseSqlServer(OptionsSnapshot.Value.DBConfig.ConnectString);
                        break;
                }
                optionsBuilder.UseLoggerFactory(new LoggerFactory(new ILoggerProvider[] { new DbContextLogProvider() }));
                base.OnConfiguring(optionsBuilder);
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var entityMethod = typeof(ModelBuilder).GetMethod("Entity", new Type[] { });
            List<Type> types = Assembly.Load("Poetry.Entity").GetTypes()
                .Where(x =>x.Name!="BaseEntity")
                .ToList();

            foreach (var type in types)
            {
               var typeBuilder= entityMethod.MakeGenericMethod(type).Invoke(modelBuilder, null) as EntityTypeBuilder;
                if (typeBuilder != null)
                {
                    typeBuilder.Ignore("Id");
                }
            }
        }
        #endregion
    }

}
