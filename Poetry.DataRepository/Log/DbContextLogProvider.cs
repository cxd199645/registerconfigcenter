﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.DataRepository.Log
{
    public class DbContextLogProvider :ILoggerProvider
    {

        public ILogger CreateLogger(string categoryName)
        {
            return new PoetryLogger();
        }

        public void Dispose()
        {
            
        }
    }
    public class PoetryLogger : ILogger
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            //只记录SQL执行日志
            if (eventId.Id == RelationalEventId.CommandExecuted.Id)
            {
                string logContent = formatter(state, exception);
            }
        }
    }
}
