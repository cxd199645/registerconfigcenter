﻿using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.DataRepository.Repository
{
  public  interface IBaseRepository<T> where T:BaseEntity
    {
        Task Save(T value);
        Task Save(List<T> list);
        Task Update(T value);
        Task Delete(T value);
        Task DeleteByExpression(Func<T, bool> expression);
        Task<T> SelectByKey(string Id);
        Task<T> SelectByExpression(Func<T, bool> expression);
         Task<List<T>> SelectListByExpression(Func<T, bool> expression);
    }
}
