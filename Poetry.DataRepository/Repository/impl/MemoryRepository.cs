﻿using Microsoft.Extensions.Caching.Memory;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.DataRepository.Repository.impl
{
    public class MemoryRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public MemoryRepository(IMemoryCache cache)
        {
            Cache = cache;
        }

        public IMemoryCache Cache { get; }
        public async Task<List<T>> GetList()
        {
            var list = await Task.Run(() =>
            {
                return Cache.Get<List<T>>(typeof(T).Name);
            }); 
            return list;
        }
        public async Task Delete(T value)
        {
            var list =await GetList();
            list.Remove(value);
            RemoveAndSet(list);
        }
        private void RemoveAndSet(List<T> list)
        {
            Cache.Remove(typeof(T).Name);
            Cache.Set(typeof(T).Name, list);
        }
        public async Task Save(T value)
        {
            var list =await GetList();
            if (list is not null && list.Count > 0)
            {
                list.Add(value);
                RemoveAndSet(list);
            }
            else
            {
                list = new List<T>();
                list.Add(value);
                RemoveAndSet(list);
            }
        }

        public async Task Save(List<T> list)
        {
            foreach (var instance in list)
            {
                await Save(instance);
            }
        }
        public async Task<T> SelectByExpression(Func<T, bool>  expression)
        {
            var list =await GetList();
            var result = list.FirstOrDefault(expression);
            return result;
        }

        public async Task<T> SelectByKey(string Id)
        {
            var list = await GetList();
            var result = list.FirstOrDefault(s => s.Id == Id);
            return result;
        }

        public async Task Update(T value)
        {
            var list = await GetList();
            var iIndex = list.FindIndex(s => s.Id == value.Id);
            if (iIndex >= 0)
            {
                list.RemoveAt(iIndex);
                list.Add(value);
                RemoveAndSet(list);
            }
        }

        public async Task<List<T>> SelectListByExpression(Func<T, bool> expression)
        {
            var list = await GetList();
            var result = list.Where(expression).ToList();
            return result;
        }

        public async Task DeleteByExpression(Func<T, bool> expression)
        {
            var list = await GetList();
            var result = list.Where(expression).ToList();
            result.ForEach(s =>
            {
                list.Remove(s);
            });
            RemoveAndSet(list);
        }
    }
}
