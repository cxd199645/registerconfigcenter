﻿using Microsoft.EntityFrameworkCore;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.DataRepository.Repository.impl
{
    public class DbContextRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public DbContextRepository(BaseDbContext baseDbContext)
        {
            BaseDbContext = baseDbContext;
            db = BaseDbContext.Set<T>();
        }
        public BaseDbContext BaseDbContext { get; }
        private readonly DbSet<T> db;

        public async Task Delete(T value)
        {
            db.Remove(value);
            await BaseDbContext.SaveChangesAsync();
        }

        public async Task Save(T value)
        {
            await db.AddAsync(value);
            await BaseDbContext.SaveChangesAsync();
        }
        public async Task Save(List<T> list)
        {
            await db.AddRangeAsync(list);
            await BaseDbContext.SaveChangesAsync();
        }
        public async Task<T> SelectByKey(string Id)
        {
            return await db.FindAsync(Id);
        }

        public async Task Update(T value)
        {
            db.Update(value);
            await BaseDbContext.SaveChangesAsync();
        }

        public async Task<T> SelectByExpression(Func<T, bool> expression)
        {
            var result = await Task.Run<T>(() =>
            {
                return db.FirstOrDefault(expression);
            });
            return result;
        }

        public async Task<List<T>> SelectListByExpression(Func<T, bool> expression)
        {
            var result = await Task.Run<List<T>>(() =>
            {
                return db.Where(expression).ToList();
            });
            return result;
        }

        public async Task DeleteByExpression(Func<T, bool> expression)
        {
            var list = db.Where(expression).ToList();
            db.RemoveRange(list);
            await BaseDbContext.SaveChangesAsync();
        }
    }
}
