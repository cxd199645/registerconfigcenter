﻿using CSRedis;
using Microsoft.Extensions.Options;
using Poetry.Common.Config;
using Poetry.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.DataRepository.Repository.impl
{
    public class RedisRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly CSRedisClient Client;
        public RedisRepository(CSRedisClient client)
        {
            Client = client;
        }
        private async Task<List<T>> GetList()
        {
            var vCount = Client.LLen(typeof(T).Name);
            var list = (await Client.LRangeAsync<T>(typeof(T).Name, 0, vCount)).ToList();
            return list;
        }
        public async Task Delete(T value)
        {
           await Client.LRemAsync(value.GetType().Name,0, value);
        }
        private async Task Delete(string Key)
        {
            await Client.DelAsync(Key);
        }
        public async Task Save(T value)
        {
           await Client.LPushAsync(value.GetType().Name, value);
        }
        public async Task Save(List<T> list)
        {
            await Client.LPushAsync(typeof(T).Name, list.ToArray());
        }
        public async Task<T> SelectByExpression(Func<T, bool>  expression)
        {
            var list =await GetList();
            var result = list.FirstOrDefault(expression);
            return result;
        }

        public async Task<T> SelectByKey(string Id)
        {
            var list = await GetList();
            var result = list.FirstOrDefault(s => s.Id == Id);
            return result;
        }

        public async Task Update(T value)
        {
            var list = await GetList();
            var iindex = list.FindIndex(s => s.Id == value.Id);
            if (iindex>=0)
            {
                list.RemoveAt(iindex);
                list.Add(value);
               await Delete(typeof(T).Name);
                await Save(list);
            }
        }

        public async Task<List<T>> SelectListByExpression(Func<T, bool> expression)
        {
            var list = await GetList();
            var result = list.Where(expression).ToList();
            return result;
        }

        public async Task DeleteByExpression(Func<T, bool> expression)
        {
            var list = await GetList();
            var result = list.Where(expression).ToList();
            result.ForEach(s =>
            {
                list.Remove(s);
            });
          await  Delete(typeof(T).Name);
            await Save(list);
        }
    }
}
