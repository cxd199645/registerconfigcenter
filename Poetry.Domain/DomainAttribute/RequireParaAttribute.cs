﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Poetry.Domain.DomainAttribute
{
  public  class RequireParaAttribute: RequiredAttribute
    {
        public RequireParaAttribute(params string[] methodName)
        {
            MethodName = methodName;
        }

        public string[] MethodName { get; }

    }
}
