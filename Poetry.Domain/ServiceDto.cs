﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain
{
   public class ServiceDto:BaseDto
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// 服务元数据
        /// </summary>
        public string ServiceMetadata { get; set; }
        /// <summary>
        /// 服务命名空间
        /// </summary>
        public string ServiceNameSpace { get; set; }
    }
}
