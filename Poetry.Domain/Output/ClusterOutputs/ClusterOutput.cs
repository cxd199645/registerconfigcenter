﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Output.ClusterOutputs
{
   public class ClusterOutput:BaseDto
    {
        public string ClusterName { get; set; }
        /// <summary>
        /// 集群Ip
        /// </summary>
        public string ClusterIp { get; set; }
        /// <summary>
        /// 集群端口
        /// </summary>
        public int ClusterPort { get; set; }
        /// <summary>
        /// 集群权重
        /// </summary>
        public string ClusterWeight { get; set; }
        /// <summary>
        /// 集群MasterId
        /// </summary>
        public string ClusterLeaderId { get; set; }
        /// <summary>
        /// 集群状态
        /// </summary>
        public int State { get; set; }
    }
}
