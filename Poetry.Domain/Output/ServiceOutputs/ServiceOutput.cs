﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Output.ServiceOutputs
{
   public class ServiceOutput:BaseDto
    {
        public string ServiceName { get; set; }
        /// <summary>
        /// 服务元数据
        /// </summary>
        public string ServiceMetadata { get; set; }
        /// <summary>
        /// 服务命名空间
        /// </summary>
        public string ServiceNameSpace { get; set; }
    }
}
