﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Output.NameSpaceOutputs
{
    public class NameSpaceOutput : BaseDto
    {
        /// <summary>
        /// 命名空间名称
        /// </summary>
        public string NameSpaceName { get; set; }
        /// <summary>
        /// 命名空间描述
        /// </summary>
        public string NameSpaceDesc { get; set; }
    }
}
