﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Output.InstanceOutputs
{
    public class InstanceOutput : BaseDto
    {
        /// <summary>
        /// 实例Ip
        /// </summary>
        public string InstanceIp { get; set; }
        /// <summary>
        /// 实例端口
        /// </summary>
        public int InstancePort { get; set; }
        /// <summary>
        /// 实例名称
        /// </summary>
        public string InstanceName { get; set; }
        /// <summary>
        /// 实例权重
        /// </summary>
        public int? InstanceWeight { get; set; }
        /// <summary>
        /// 元数据
        /// </summary>
        public string InstanceMetaData { get; set; }
        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// 健康检查时间间隔
        /// </summary>
        public int? HealthCheckTimeInterval { get; set; }
        /// <summary>
        /// 健康状态
        /// </summary>
        public bool? IsHealth { get; set; }
        /// <summary>
        /// 是否上线
        /// </summary>
        public bool? Enabled { get; set; }
    }
}
