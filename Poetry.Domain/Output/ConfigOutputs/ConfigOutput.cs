﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Output.ConfigOutputs
{
    /// <summary>
    /// 
    /// </summary>
  public  class ConfigOutput : BaseDto
    {
        /// <summary>
        /// 配置名称
        /// </summary>
        public string ConfigName { get; set; }
        /// <summary>
        /// 配置描述
        /// </summary>
        public string ConfigDesc { get; set; }
        /// <summary>
        /// 配置数据
        /// </summary>
        public string ConfigData { get; set; }
        /// <summary>
        /// 所属环境
        /// </summary>
        public string EnvironmentName { get; set; }
    }
}
