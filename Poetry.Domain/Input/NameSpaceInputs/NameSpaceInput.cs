﻿using Poetry.Domain.DomainAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Input.NameSpaceInputs
{
    public class NameSpaceInput : BaseDto
    {
        /// <summary>
        /// 命名空间名称
        /// </summary>
        [RequirePara("InsertNameSpace")]
        public string NameSpaceName { get; set; }
        /// <summary>
        /// 命名空间描述
        /// </summary>
        public string NameSpaceDesc { get; set; }
    }
}
