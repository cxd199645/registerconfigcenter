﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Input.ClusterInputs
{
   public class ClusterInput:BaseDto
    {
        /// <summary>
        /// 集群名称
        /// </summary>
        public string ClusterName { get; set; }
        /// <summary>
        /// 集群Ip
        /// </summary>
        public string ClusterIp { get; set; }
        /// <summary>
        /// 集群端口
        /// </summary>
        public int ClusterPort { get; set; }
        /// <summary>
        /// 集群权重
        /// </summary>
        public string ClusterWeight { get; set; }
        /// <summary>
        /// Leader名称
        /// </summary>
        public string MasterIp{ get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int State { get; set; }
    }
}
