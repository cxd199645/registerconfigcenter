﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Input.EnvInputs
{
   public class EnvironmentInput : BaseDto
    {
        /// <summary>
        /// 环境名称
        /// </summary>
        public string EnvironmentName { get; set; }
        /// <summary>
        /// 环境描述
        /// </summary>
        public string EnvironmentDesc { get; set; }
    }
}
