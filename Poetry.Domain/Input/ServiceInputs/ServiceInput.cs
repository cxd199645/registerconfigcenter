﻿using Poetry.Domain.DomainAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poetry.Domain.Input.ServiceInputs
{
   public class ServiceInput:BaseDto
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        [RequirePara("RegisterService", "CancelService", "UpdateService")]
        public string ServiceName { get; set; }
        /// <summary>
        /// 服务元数据
        /// </summary>
        public string ServiceMetadata { get; set; }
        /// <summary>
        /// 服务命名空间
        /// </summary>
        public string ServiceNameSpace { get; set; }
    }
}
